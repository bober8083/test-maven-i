import dayone.TestObj;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TestObjTest {
    @Test
    public void test_1() {
        TestObj test = new TestObj(1);
        Assertions.assertEquals(test.getParam(), 1);
    }
}