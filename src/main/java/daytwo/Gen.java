package daytwo;

public class Gen {
    private String my;
    private String test;
    private String test2;
    private String test3;
    private String test54;
    private String test64;
    private String test6;
    private String test334;
    private String test237;
    private String test634;
    private String test2352;
    private String test4;
    private String test346;
    private String testds3;
    private String test3457;

    Gen(String my, String test, String test2,
        String test3, String test54, String test64,
        String test6, String test334, String test237,
        String test634, String test2352, String test4,
        String test346, String testds3, String test3457) {
        this.my = my;
        this.test = test;
        this.test2 = test2;
        this.test3 = test3;
        this.test54 = test54;
        this.test64 = test64;
        this.test6 = test6;
        this.test334 = test334;
        this.test237 = test237;
        this.test634 = test634;
        this.test2352 = test2352;
        this.test4 = test4;
        this.test346 = test346;
        this.testds3 = testds3;
        this.test3457 = test3457;
    }

    public static GenBuilder builder() {
        return new GenBuilder();
    }

    public String getMy() {
        return this.my;
    }

    public String getTest() {
        return this.test;
    }

    public String getTest2() {
        return this.test2;
    }

    public String getTest3() {
        return this.test3;
    }

    public String getTest54() {
        return this.test54;
    }

    public String getTest64() {
        return this.test64;
    }

    public String getTest6() {
        return this.test6;
    }

    public String getTest334() {
        return this.test334;
    }

    public String getTest237() {
        return this.test237;
    }

    public String getTest634() {
        return this.test634;
    }

    public String getTest2352() {
        return this.test2352;
    }

    public String getTest4() {
        return this.test4;
    }

    public String getTest346() {
        return this.test346;
    }

    public String getTestds3() {
        return this.testds3;
    }

    public String getTest3457() {
        return this.test3457;
    }

    public String toString() {
        return "Gen(my=" + this.getMy()
                + ", test=" + this.getTest()
                + ", test2=" + this.getTest2()
                + ", test3=" + this.getTest3()
                + ", test54=" + this.getTest54()
                + ", test64=" + this.getTest64()
                + ", test6=" + this.getTest6()
                + ", test334=" + this.getTest334()
                + ", test237=" + this.getTest237()
                + ", test634=" + this.getTest634()
                + ", test2352=" + this.getTest2352()
                + ", test4=" + this.getTest4()
                + ", test346=" + this.getTest346()
                + ", testds3=" + this.getTestds3()
                + ", test3457=" + this.getTest3457() + ")";
    }

    public static class GenBuilder {
        private String my;
        private String test;
        private String test2;
        private String test3;
        private String test54;
        private String test64;
        private String test6;
        private String test334;
        private String test237;
        private String test634;
        private String test2352;
        private String test4;
        private String test346;
        private String testds3;
        private String test3457;

        GenBuilder() {
        }

        public daytwo.Gen.GenBuilder my(String my) {
            this.my = my;
            return this;
        }

        public daytwo.Gen.GenBuilder test(String test) {
            this.test = test;
            return this;
        }

        public daytwo.Gen.GenBuilder test2(String test2) {
            this.test2 = test2;
            return this;
        }

        public daytwo.Gen.GenBuilder test3(String test3) {
            this.test3 = test3;
            return this;
        }

        public daytwo.Gen.GenBuilder test54(String test54) {
            this.test54 = test54;
            return this;
        }

        public daytwo.Gen.GenBuilder test64(String test64) {
            this.test64 = test64;
            return this;
        }

        public daytwo.Gen.GenBuilder test6(String test6) {
            this.test6 = test6;
            return this;
        }

        public daytwo.Gen.GenBuilder test334(String test334) {
            this.test334 = test334;
            return this;
        }

        public daytwo.Gen.GenBuilder test237(String test237) {
            this.test237 = test237;
            return this;
        }

        public daytwo.Gen.GenBuilder test634(String test634) {
            this.test634 = test634;
            return this;
        }

        public daytwo.Gen.GenBuilder test2352(String test2352) {
            this.test2352 = test2352;
            return this;
        }

        public daytwo.Gen.GenBuilder test4(String test4) {
            this.test4 = test4;
            return this;
        }

        public daytwo.Gen.GenBuilder test346(String test346) {
            this.test346 = test346;
            return this;
        }

        public daytwo.Gen.GenBuilder testds3(String testds3) {
            this.testds3 = testds3;
            return this;
        }

        public daytwo.Gen.GenBuilder test3457(String test3457) {
            this.test3457 = test3457;
            return this;
        }

        public daytwo.Gen build() {
            return new Gen(my, test, test2,
                    test3, test54, test64, test6,
                    test334, test237, test634,
                    test2352, test4, test346,
                    testds3, test3457);
        }

        public String toString() {
            return "Gen.GenBuilder(my=" + this.my + ", test="
                    + this.test + ", test2=" + this.test2 + ", test3="
                    + this.test3 + ", test54=" + this.test54 + ", test64="
                    + this.test64 + ", test6=" + this.test6 + ", test334="
                    + this.test334 + ", test237=" + this.test237 + ", test634="
                    + this.test634 + ", test2352=" + this.test2352 + ", test4="
                    + this.test4 + ", test346=" + this.test346 + ", testds3="
                    + this.testds3 + ", test3457=" + this.test3457 + ")";
        }
    }
}
