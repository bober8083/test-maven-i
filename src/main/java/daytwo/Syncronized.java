package daytwo;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

public class Syncronized {
    public static void main(String[] args) {
        Gen gen = Gen.builder()
                .test("121")
                .test3("23532")
                .test634("37354")
                .test4("test346")
                .build();

        Syncronized app = new Syncronized();
        app.runStaticInside();

        ConcurrentHashMap<Integer, Integer> mp =
                new ConcurrentHashMap<Integer, Integer>();

        Syncronized app1 = new Syncronized();
        Syncronized app2 = new Syncronized();

        runObjInside(app1, app2);

        AtomicInteger s = new AtomicInteger(1);
        int s1 = s.addAndGet(10);

        CompletableFuture<Integer> completableFuture = CompletableFuture.supplyAsync(() -> {
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return 100;
        });

        try {
            completableFuture.get(15, TimeUnit.MINUTES);
            System.out.println("res: " + s);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }

    public void runStaticInside() {
        for (int i = 0; i < 1000; ++i) {
            final int j = i;
            if (j % 2 == 0) {
                Thread t = new Thread(() -> {
                    my1(j);
                });
                t.start();
            } else {
                Thread t = new Thread(() -> {
                    my2(j);
                });
                t.start();
            }
        }

        System.out.println("wait");
    }

    public static void runObjInside(Syncronized a1, Syncronized a2) {
        for (int i = 0; i < 1000; ++i) {
            final int j = i;
            if (j % 2 == 0) {
                Thread t = new Thread(() -> {
                    a1.my1ex(j);
                });
                t.start();
            } else {
                Thread t = new Thread(() -> {
                    a2.my2ex(j);
                });
                t.start();
            }
        }

        System.out.println("wait");
    }

    public static synchronized void my1(int arg) {
        try {
            System.out.println("started 1 " + arg);
            Thread.sleep(10);
            System.out.println("finished 1 " + arg);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public static synchronized void my2(int arg) {
        try {
            System.out.println("started 2 " + arg);
            Thread.sleep(10);
            System.out.println("finished 2 " + arg);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public synchronized void my1ex(int arg) {
        try {
            System.out.println("started 1 " + arg);
            Thread.sleep(3);
            System.out.println("finished 1 " + arg);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public synchronized void my2ex(int arg) {
        try {
            System.out.println("started 2 " + arg);
            Thread.sleep(3);
            System.out.println("finished 2 " + arg);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
