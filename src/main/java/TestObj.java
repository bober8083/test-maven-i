import java.util.ArrayList;
import java.util.List;

public class TestObj {
    public synchronized int getParam() {
        return param;
    }

    private final int param;
    private final List<String> lst;

    public TestObj(int param) {
        this.param = param;
        lst = new ArrayList<>();
    }

    public TestObj(int param, List<String> lst) {
        this.param = param;
        this.lst = new ArrayList<>(lst);
    }
}
