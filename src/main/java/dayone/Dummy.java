package dayone;

import lombok.SneakyThrows;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Dummy {
    private static final Logger logger = LogManager.getLogger(Dummy.class);

    private static final List<String> USERS =
            Stream.of("Denis", "Sergey", "Emilia", "Zulfiya", "Valentin", "Pavel")
                    .collect(Collectors.toList());
    private static final int COUNT = 35000;

    public void create(String msg) {
        logger.error("Test {}", msg);
    }

    @SneakyThrows
    public static void main(String[] args) {
        logger.atLevel(Level.ALL);
        Dummy dummy = new Dummy();
        dummy.runPgInsert();
    }

    public void runPgInsert() throws SQLException {
        String url = "jdbc:postgresql://localhost:5432/postgres";
        Properties props = new Properties();
        props.setProperty("user", "test");
        props.setProperty("password", "test");

        try (Connection con = DriverManager.getConnection(url, props)) {
            runInsertSlow(con);
            runInsertFast(con);
            runReading(con);
        }
    }

    private List<User> runReading(Connection con) {
        String query = "select \"ID\", \"NAME\", \"DETAILS\" from \"STUD\".\"USER\" ORDER BY 1 ";
        List<User> res = new ArrayList<>();

        try (Statement stmt = con.createStatement()) {
            try (ResultSet rs = stmt.executeQuery(query)) {
                while (rs.next()) {
                    Long id = rs.getLong("ID");
                    String name = rs.getString("NAME");
                    String details = rs.getString("DETAILS");
                    User newUser = new User(id, name, details);
                    res.add(newUser);
                }
            }
        } catch (SQLException e) {
            logger.error("Error!", e);
        }

        return res;
    }

    private void runInsertSlow(Connection con) throws SQLException {
        Duration duration = new Duration();

        Statement stmt = con.createStatement();
        int from = 1110000;

        for (int i = from; i < (from + COUNT); ++i) {
            String currentUser = USERS.get(i % USERS.size()) + " " + String.format("%d", i);

            String query = String.format("INSERT INTO \"STUD\".\"USER\" "
                            + " ( \"ID\", \"NAME\", \"DETAILS\" ) "
                            + " VALUES (%d, \'%s\', \'%s\')",
                    i, currentUser, RandomStringUtils.randomAlphanumeric(10));
            stmt.executeUpdate(query);
        }

        duration.timeInMs("Slow");
    }

    private void runInsertFast(Connection con) throws SQLException {
        con.setAutoCommit(false);

        Duration duration = new Duration();

        String query = "INSERT INTO \"STUD\".\"USER\" "
                        + " ( \"ID\", \"NAME\", \"DETAILS\" ) "
                        + " VALUES (?, ?, ?)";

        PreparedStatement stmt = con.prepareStatement(query);
        int from = 1;

        for (int i = from; i < (from + COUNT); ++i) {
            String currentUser = USERS.get(i % USERS.size());
            String details = RandomStringUtils.randomAlphanumeric(10);

            stmt.setInt(1, i);
            stmt.setString(2, currentUser);
            stmt.setString(3, details);

            stmt.executeUpdate();
        }

        con.commit();
        duration.timeInMs("Fast");
    }

    public void runPg() throws SQLException {
        String url = "jdbc:postgresql://localhost:5432/postgres";
        Properties props = new Properties();
        props.setProperty("user", "test");
        props.setProperty("password", "test");
        Connection con = DriverManager.getConnection(url, props);
        runWithConnection(con);
        con.close();
    }

    public void runMaria() throws SQLException {
        String url = "jdbc:mariadb://localhost:3306/test";
        Properties props = new Properties();
        props.setProperty("user", "test2");
        props.setProperty("password", "test2");
        Connection con = DriverManager.getConnection(url, props);
        runWithConnection(con);
        con.close();
    }

    private void runWithConnection(Connection con) {
        String query = "select * from `stud.mytab`";
        //String query = "select * from \"STUD\".\"MYTAB\"";
        try (Statement stmt = con.createStatement()) {
            try (ResultSet rs = stmt.executeQuery(query)) {
                while (rs.next()) {
                    Long id = rs.getLong("ID");
                    Number n = rs.getLong("NAME");
                    System.out.println(id);
                }
            }
        } catch (SQLException e) {
            logger.error("Error!", e);
        }
    }

    private String buildLargeString() {
        return RandomStringUtils.randomAlphabetic(25);
    }
}
