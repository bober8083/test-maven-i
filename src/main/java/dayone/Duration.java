package dayone;

public class Duration {
    long start;

    public Duration() {
        start = System.currentTimeMillis();
    }

    public long timeInMs(String msg) {
        long res = System.currentTimeMillis() - start;
        System.out.println(msg + " took " + res + " ms");
        return res;
    }
}
