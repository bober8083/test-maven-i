import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        long from = System.currentTimeMillis();
        StringBuilder bld = new StringBuilder();
        try (Scanner s = new Scanner(new File("d:/baskervilles.txt"))) {
            while (s.hasNext()) {
                String newLine = s.next();
                // так не делаем!!
                // bld = bld + newLine;
                // bld = bld + " ";
                bld.append(newLine);
                bld.append(" ");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        long to = System.currentTimeMillis();
        System.out.println(to - from + "ms");
        System.out.println("");

        String txt = bld.toString();
        List<String> uniqWords = Stream.of(txt.split(" "))
                .filter(f -> f != null)
                .filter(f -> f.length() != 0)
                .map(f -> f.toLowerCase(Locale.ENGLISH))
                .map(f -> removeComasAndDots(f))
                .filter(f -> f.length() != 0)
                .distinct()
                .collect(Collectors.toList());

        Stream.of(txt.split(" "))
                .filter(f -> f != null)
                .filter(f -> f.length() != 0)
                .map(f -> repaceIfRequired(f))
                .forEach(f -> System.out.print(f + " "));

        System.out.println("");

        TestObj testObj1 = new TestObj(11);

        Thread thread1 = new Thread(() -> {
            System.out.println("t1");
            testObj1.getParam();
        });

        Thread thread2 = new Thread(() -> {
            System.out.println("t2");
            testObj1.getParam();
        });

        thread1.start();
        thread2.start();

        try {
            thread2.join(200000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < 1000; ++i) {
            final int j = i;
            ForkJoinPool.commonPool().submit(() -> {
                System.out.println("" + j);
                System.out.println("id = " + Thread.currentThread().getId());
                testObj1.getParam();
            });
        }

        try {
            Thread.sleep(100000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("test");
    }

    private static String removeComasAndDots(String param) {
        if (param == null || param.length() == 0) {
            return param;
        }
        param = param.replace(".", "");
        param = param.replace(",", "");
        param = param.replace("!", "");
        param = param.replace("?", "");
        return param;
    }

    private static String repaceIfRequired(String f) {
        if ("go".equals(f)) {
            return "run!";
        }

        return f;
    }
}
