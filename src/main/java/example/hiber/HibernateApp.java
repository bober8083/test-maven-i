package example.hiber;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.hibernate.stat.Statistics;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Slf4j
public class HibernateApp {

    private static final Random RANDOM = new Random();

    public static void main(String[] args) {
        HibernateApp theApp = new HibernateApp();
        log.info("Startup!");
        System.err.println(log.isTraceEnabled());
        System.err.println(log.isDebugEnabled());
        System.err.println(log.isInfoEnabled());
        System.err.println(log.isWarnEnabled());
        System.err.println(log.isErrorEnabled());

        String fileName = "D:/java2/maven-ii/test-maven-i/src/main/resources/hibernate.properties";
        try (SessionFactory sessionFactory = HibernateUtils.getSessionFactory(fileName)) {
            try (Session session = sessionFactory.openSession()) {
                //theApp.removeUsersUsingNativeQuery(session);
                theApp.createUsersObj(session);
                theApp.selectUsers(session);
                theApp.selectSalary(session);
            }

            Statistics stats = sessionFactory.getStatistics();
            long queryCount = stats.getQueryExecutionCount();
            long collectionFetchCount = stats.getCollectionFetchCount();

            log.info("Queries count {}", queryCount);
            log.info("Collections fetch count {}", collectionFetchCount);
        }
    }

    private void selectSalary(Session session) {
        Query query = session.createQuery("from Salary");
        List<example.hiber.Salary> list = query.list();
        log.info("Salary list: {}", list);
    }

    private void removeUsersUsingNativeQuery(Session session) {
        Transaction transaction = session.beginTransaction();
        session.createNativeQuery("delete from \"STUD\".\"USER\"").executeUpdate();
        transaction.commit();
    }

    private void createUsersObj(Session session) {
        Transaction transaction = session.beginTransaction();

        User u1 = new User();
        u1.name = "My name";
        u1.details = "Super details";

        session.persist(u1);
        fillUpSalary(session, u1);

        User u2 = new User();
        u2.name = "My name - 2";
        u2.details = "Super details - 2";
        session.persist(u2);
        fillUpSalary(session, u2);

        User u3 = new User();
        u3.name = "My name - 3";
        u3.details = "Super details - 3";
        session.persist(u3);
        fillUpSalary(session, u3);

        transaction.commit();
    }

    private void fillUpSalary(Session session, User user) {
        user.salaries = new ArrayList<>();

        for (int i = 0; i < 11; ++i) {
            Salary salary = new Salary();
            salary.month = i + 1;
            salary.amount = BigDecimal.valueOf(1.424 + RANDOM.nextInt(200) * 0.1);
            user.salaries.add(salary);
            salary.userid = user.id;
            session.persist(salary);
        }
    }

//    private void selectUsers() {
//        String selectQuery = "SELECT student FROM Student student";
//        TypedQuery<User> selectFromStudentTypedQuery = em.createQuery(selectQuery, User.class);
//        List<User> students = selectFromStudentTypedQuery.getResultList();
//        System.out.println(students);
//    }

    private void selectUsers(Session session) {
        Query query = session.createQuery("from User");
        List<example.hiber.User> list = query.list();
        log.info("Users: {}", list);
    }
}

