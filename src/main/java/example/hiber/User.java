package example.hiber;

import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "\"STUD\".\"USER\"")
@ToString
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_seq")
    @SequenceGenerator(name = "user_seq",
            sequenceName = "\"STUD\".\"user_sequence\"",
            initialValue = 1, allocationSize = 20)
    @Column(name = "\"ID\"", unique = true)
    public Long id;

    @Column(name = "\"NAME\"")
    public String name;

    @Column(name = "\"DETAILS\"")
    public String details;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "\"USERID\"")
    public List<Salary> salaries;
}
