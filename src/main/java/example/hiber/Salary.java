package example.hiber;

import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "\"STUD\".\"SALARY\"")
@ToString
public class Salary {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "salary_seq")
    @SequenceGenerator(name = "salary_seq",
            sequenceName = "\"STUD\".\"salary_sequence\"",
            initialValue = 1, allocationSize = 2)
    @Column(name = "\"ID\"", nullable = false)
    public Long id;

    @Column(name = "\"AMOUNT\"")
    public BigDecimal amount;

    @Column(name = "\"MONTH\"", nullable = false)
    public Integer month;

    @Column(name = "\"USERID\"", nullable = false)
    public Long userid;
}
