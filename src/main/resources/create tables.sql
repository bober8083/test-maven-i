CREATE SCHEMA IF NOT EXISTS "STUD"
    AUTHORIZATION postgres;

GRANT ALL ON SCHEMA "STUD" TO postgres;

GRANT ALL ON SCHEMA "STUD" TO test;



-- Table: STUD.SALARY

-- DROP TABLE IF EXISTS "STUD"."SALARY";

CREATE TABLE IF NOT EXISTS "STUD"."SALARY"
(
    "MONTH" integer,
    "USERID" bigint,
    "AMOUNT" numeric,
    "ID" bigint
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS "STUD"."SALARY"
    OWNER to postgres;

GRANT ALL ON TABLE "STUD"."SALARY" TO postgres;

GRANT ALL ON TABLE "STUD"."SALARY" TO test;


-- Table: STUD.USER

-- DROP TABLE IF EXISTS "STUD"."USER";

CREATE TABLE IF NOT EXISTS "STUD"."USER"
(
    "ID" bigint,
    "NAME" text COLLATE pg_catalog."default",
    "DETAILS" text COLLATE pg_catalog."default"
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS "STUD"."USER"
    OWNER to postgres;

GRANT ALL ON TABLE "STUD"."USER" TO postgres;

GRANT ALL ON TABLE "STUD"."USER" TO test;


-- SEQUENCE: STUD.salary_sequence

-- DROP SEQUENCE IF EXISTS "STUD".salary_sequence;

CREATE SEQUENCE IF NOT EXISTS "STUD".salary_sequence
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE "STUD".salary_sequence
    OWNER TO postgres;

GRANT ALL ON SEQUENCE "STUD".salary_sequence TO postgres;

GRANT ALL ON SEQUENCE "STUD".salary_sequence TO test;


-- SEQUENCE: STUD.user_sequence

-- DROP SEQUENCE IF EXISTS "STUD".user_sequence;

CREATE SEQUENCE IF NOT EXISTS "STUD".user_sequence
    CYCLE
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE "STUD".user_sequence
    OWNER TO postgres;

GRANT ALL ON SEQUENCE "STUD".user_sequence TO postgres;

GRANT ALL ON SEQUENCE "STUD".user_sequence TO test;